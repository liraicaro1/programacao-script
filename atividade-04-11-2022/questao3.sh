#!/bin/bash

function menu () {
echo '	 --------------->Menu<---------------
	Digite "sel" para selecionar um arquivo
	Digite "ver" para verificar se o arquivo existe
	Digite "bkp" para fazer backup do arquivo
	Digite "rm" para remover a última linha do arquivo
	Digite "con" para confirmar se quer continuar
	Digite "menu" para exibir o menu novamente
	Digite "limpar" para limpar a tela
'
}
menu
while true 
do
read -p "Digite a opção: " opc

if [ "$opc" = "sel" ] 
then
	read -p "Digite o nome do arquivo: " arq
elif [ "$opc" = "ver" ]
then 
	if [ -f $arq ]
	then 
		echo "O arquivo $arq existe"
	else 
		echo "O arquivo $arq não existe"
	fi
elif [ "$opc" = "bkp" ]
then
	cp $arq $arq.bkp

elif [ "$opc" = "rm" ]
then
	sed '$d' $arq
elif [ "$opc" = "con" ]
then
	read -p "Deseja continuar [s/n] ABENÇOADO ? " cmd
	if [ "$cmd" = "s" ]
	then 
		sleep 2 
		echo "Continuando ..."
	fi
	if [ "$cmd" = "n" ]
	then 
		echo "Xau..."
		break
	fi
elif [ "$opc" = "menu" ]
then
	menu
elif [ "$opc" = "limpar" ]
then 
	clear
fi
done
