#!/bin/bash
echo -e '\033[1;31;47m$0 Retorna o nome do programa executado.'
echo -e "Seu conteúdo: $0"
echo -e '$1 Substitui o valor do primeiro parâmetro.'
echo -e "Seu conteúdo: $1"
echo -e '$2 Substitui o valor do segundo parâmetro.'
echo -e "Seu conteúdo: $2"
echo -e '$# Retorna o número de argumentos que o programa recebeu.'
echo -e "Seu conteúdo: $#"
echo -e '$* Retorna todos os argumentos informados na execução do programa.'
echo -e "Seu conteúdo: $*" "\033[0m"
