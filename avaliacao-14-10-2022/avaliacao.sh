#!/bin/bash

while true 
do	
	echo '------------------------------Menu------------------------------
	Digite "sair" para sair do programa
	Digite "memoria" para informações da memória
       	Digite "disco" para informações de uso do disco
	Digite "cpu" para informações gerais da CPU
	Digite "verificar" para confirmar se um arquivo existe
	Digite "exibir" para exibir o diretório atual
	Digite "listar" para listar os arquivos do diretório atual
	Digite "mudar" para mudar o diretório atual	
	'
	read -p "Digite a opção: " opc
	if [ $opc = "sair" ]
	then
	       echo "###############Encerrando programa###############"	
		break	 
	elif [ $opc = "memoria" ]
	then 
		echo "----->Informações da memória<-----" 
		vmstat
	elif [ $opc = "disco" ]
	then
		echo "----->Informações de uso do disco<-----" 
		df -h
	elif [ $opc = "cpu" ]
	then
		echo "----->Informações da CPU<-----"
	       	cpuinfo	
	elif [ $opc = "verificar" ]
	then
		read -p "Digite o nome do arquivo: " arq
		echo "----->Verificando se o arquivo existe<-----"
		sleep 1
		echo .
		sleep 1
		echo .
		sleep 2
		echo .
		if [ -f $arq ]
		then
			echo "O arquivo $arq existe"
		else
			echo "O arquivo $arq não existe"
		fi
	elif [ $opc = "exibir" ]
	then
		echo "----->Diretório atual<-----"
		pwd 
	elif [ $opc = "listar" ]
	then
		echo "----->Arquivos do diretório atual<-----"
		ls
	elif [ $opc = "mudar" ]
	then
		read -p "Digite o diretório para abrir: " dir
		echo "---->Mudando de diretório<-----"	
		echo .
		sleep 1
		echo .
		sleep 2
		echo .
		cd $dir
	else
		echo .
		sleep 1
		echo .
		sleep 1
		echo .
		sleep 1
		echo .

		echo "----->Opção não encontrada<-----"
	fi

done
