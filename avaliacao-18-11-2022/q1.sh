#!/bin/bash

N=$1
Tam=$( cat texto.txt | wc -l )

if [ $N -eq $Tam ] || [ $N -lt $Tam ]
then
	sort -R texto.txt | head -n $N
else
	echo "A quantidade de linhas solicitadas é superior ao tamanho do arquivo de texto. Digite um número menor ou igual a $Tam."
fi

