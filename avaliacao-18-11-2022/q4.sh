#!/bin/bash

while true 
do	
	echo '------------------------------Menu------------------------------
	Digite "sair" para sair do programa
	Digite "selecionar" para selecionar um arquivo
       	Digite "preview" para ver um preview do arquivo selecionado
	Digite "criar" criar um arquivo com N linhas de blablabla
	Digite "verificar" para confirmar se um arquivo existe
	Digite "exibir" para exibir o arquivo selecionado
	Digite "listar" para listar os arquivos do diretório atual
	Digite "mudar" para mudar o diretório atual	
	'
	read -p "Digite a opção: " opc
	if [ $opc = "sair" ]
	then
	       echo "###############Encerrando programa###############"	
		break	 
	elif [ $opc = "selecionar" ]
	then 
		read -p "Digite o nome de um arquivo: " arq
		echo "----->Arquivo $arq selecionado<-----"

	elif [ $opc = "preview" ]
	then
		echo "----->Exibindo preview do arquivo $arq<-----" 
		head -n 2 $arq 
		echo "[...]"
		tail -n 2 $arq
	elif [ $opc = "criar" ]
	then
		read -p "Digite a quantidade de linhas para criar o arquivo: " qtd
		echo "----->Criando arquivo BLABLABLA<-----"
		sort -R texto.txt | head -n $qtd | touch bla
	elif [ $opc = "verificar" ]
	then
		read -p "Digite o nome do arquivo: " arq
		echo "----->Verificando se o arquivo existe<-----"
		sleep 1
		echo .
		sleep 1
		echo .
		sleep 2
		echo .
		if [ -f $arq ]
		then
			echo "O arquivo $arq existe"
		else
			echo "O arquivo $arq não existe"
		fi
	elif [ $opc = "exibir" ]
	then
		echo "----->Exibindo arquivo selecionado: $arq<-----"
		cat $arq
	elif [ $opc = "listar" ]
	then
		echo "----->Arquivos do diretório atual<-----"
		ls
	elif [ $opc = "mudar" ]
	then
		read -p "Digite o diretório para abrir: " dir
		echo "---->Mudando de diretório<-----"	
		echo .
		sleep 1
		echo .
		sleep 2
		echo .
		cd $dir
	else
		echo .
		sleep 1
		echo .
		sleep 1
		echo .
		sleep 1
		echo .

		echo "----->Opção não encontrada<-----"
	fi

done

