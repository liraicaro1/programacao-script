#!/bin/bash
echo Redirecionadores
echo "> - Direciona a saída de um comando para um arquivo e sobrescreve o conteúdo dele. Cria o arquivo caso não exista. Ex: history > texto."
echo ">> - Direciona a saída de um comando para um arquivo e não sobrescreve o conteúdo, adicionando a saída do comando ao final do conteúdo do arquivo. Cria o arquivo caso não exista. Ex: ls /tmp  >> texto."
echo "< - Direciona o conteúdo de um arquivo para a entrada de um comando. Ex: cat < /etc/hostname."
echo "2> - Direciona a saída de erro (stderr) para dentro de um arquivo, sobrescreve o conteúdo. Cria o arquivo caso não exista. Ex: chicho 2> arq."
echo "2>> - Direiona a saída de erro (stderr) para um arquivo e adiciona a saída de erro ao final do conteúdo do arquivo. Ex: CHICHO 2>> arq."
echo "&> - Direciona o stdout (1) e o stderr(2) para um arquivo, sobrescreve o conteúdo. Cria o arquivo caso não exista. Ex: cat /etc/* &> arq2."
echo "&>> - Direciona o stdout(1) e o stderr (2) para um arquivo e não sobrescreve o conteúdo. Cria o arquivo caso não exista. Ex: cat /etc/* &>> arq2."
echo "| - Direciona a saída de um comando para a entrada de outro. Ex: more /etc/passwd | tee arq3."
