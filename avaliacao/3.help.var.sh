#!/bin/bash
echo "Variáveis automáticas"
echo '$USER - Retorna o nome do usuário. Ex: echo $USER' 
echo '$UID - Retorna o ID do usuário. Ex: echo $UID'
echo '$PWD - Retorna o diretório atual. Ex: ls $PWD'
echo '$HOME - Retorna o diretório home do usuário atual. Ex: ls $HOME'
echo '$HISTFILE - Nome do arquivo que o histórico de comandos é salvo . Ex: echo $HISTFILE'
echo '$HOSTNAME - Nome do computador. Ex: echo $HOSTNAME'
echo '$PPID - Retorna o ID do processo pai. Ex: echo $PPID'
echo '$RANDOM - Retorna um número aleatório entre 0 e 32767. Ex: echo $RANDOM'
echo '$LINENO - Retorna o número da linha que o script está executando. Ex: echo $LINENO'
echo '$SECONDS - Retorna quantos segundos o script está rodando. Ex: echo $SECONDS'
echo '$0 - Retorna o nome do script. Ex: echo $0'
echo '$# - Retorna o número de parâmetros. Ex: echo $#'
echo '$! - Retorna o ID de uma tarefa em background. Ex: echo $!'
echo '$? - Retorna o status de saída ou de erro. Ex: echo $?'
echo '$_ - Retorna o argumento do comando anterior. Ex $_'

