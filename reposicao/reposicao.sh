#!/bin/bash

while true 
do	
	echo '------------------------------Menu------------------------------
	Digite "sair" para sair do programa
	Digite "selecionar" para selecionar um diretório
       	Digite "RM" para renomear todos os arquivos colocando todas as letras em maiúsculo
       	Digite "rm" para renomear todos os arquivos colocando todas as letras em minúsculo
	Digite "criar" criar um arquivo com N linhas de blablabla
	Digite "lsd" para listar apenas os subdiretórios do diretório atual
	Digite "listar" para listar os arquivos do diretório atual
	Digite "mudar" para mudar o diretório atual	
	Digite "lae" para listar os arquivos executáveis do diretório atual
	'
	read -p "Digite a opção: " opc
	if [ $opc = "sair" ]
	then
	       echo "###############Encerrando programa###############"	
		break	 
	elif [ $opc = "selecionar" ]
	then
		read -p "Digite o nome de um diretório: " dir
		echo "----->Diretório $dir selecionado<-----"
		cd $dir

	elif [ $opc = "RM" ]
	then
		echo "----->Renomeando todos os arquivos colocando todas as letras maiúsculas<-----" 
		ls -l | grep '^-' | awk '{print $NF}' | rename -v 'y/a-z/A-Z/'
	elif [ $opc = "criar" ]
	then
		read -p "Digite a quantidade de linhas para criar o arquivo: " qtd
		echo "----->Criando arquivo BLABLABLA<-----"
		sort -R texto.txt | head -n $qtd > bla
	elif [ $opc = "lsd" ]
	then
		echo "----->Exibindo subdiretórios do diretório atual<-----"
		ls -d */
	elif [ $opc = "rm" ]
	then
		echo "----->Renomeando todos os arquivos colocando todas as letras minúsculas<-----" 
		ls -l | grep '^-' | awk '{print $NF}' | rename -v 'y/A-Z/a-z/'
	elif [ $opc = "listar" ]
	then
		echo "----->Arquivos do diretório atual<-----"
		ls -l | grep '^-'
	elif [ $opc = "lae" ]
	then
		echo "----->Arquivos executáveis do diretório atual<-----"
		ls -l | grep '^-..x'
	elif [ $opc = "mudar" ]
	then
		echo "----->Renomeando todos os arquivos com extensão .txt para .docx;<-----"
		ls *.txt | rename s'/.txt/.docx/'
	fi	

done
